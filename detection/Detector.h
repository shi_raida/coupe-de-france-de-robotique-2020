/**
 * @name: Detector.h
 * @author: Roland Godet <r.godet@outlook.fr>
 * @date: 23/01/2020
 */

#ifndef DETECTION_DETECTOR_H
#define DETECTION_DETECTOR_H


#include <string>
#include <opencv/cv.hpp>
#include <opencv2/aruco.hpp>


class Detector {
public:
  Detector();

  void detect();
  void loadDict(int dictId);
  bool loadImg(const std::string& imgPath);
  bool loadVideo(const std::string& videoPath = "");
  void setCamera(int id);
  void setShow(bool show);
  void show(bool pause = false);
  bool startVideo();

private:
  bool loadImgFromVideo();
  bool testImgLoaded(const std::string& errMsg, const std::string& sucMsg);

  int m_cameraId = 0;
  cv::Mat m_img;
  cv::VideoCapture m_video;
  cv::Ptr<cv::aruco::Dictionary> m_dict;
  std::vector<int> m_markersIds;
  std::vector<std::vector<cv::Point2f>> m_markersCorners;
  bool m_show = false;
};


#endif //DETECTION_DETECTOR_H
