/**
 * @name: utility.cpp
 * @author: Roland Godet <r.godet@outlook.fr>
 * @date: 23/01/2020
 */

#include <iostream>
#include "utility.h"

using namespace std;


void debug(const string& s) {
#ifdef DEBUG
  cout << "[DEBUG] " << s << endl;
#endif
}