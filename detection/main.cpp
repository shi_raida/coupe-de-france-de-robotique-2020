/**
 * @name: main.cpp
 * @author: Roland Godet <r.godet@outlook.fr>
 * @date: 22/01/2020
 *
 * @brief: Logiciel de détection pour la Coupe de France de Robotique de 2020 pour l'équipe du [Kro]Bot.
 */

#include "Detector.h"

using namespace cv::aruco;


int main(int argc, char *argv[]) {
  Detector detector = Detector();
  detector.loadDict(DICT_4X4_100);
  detector.setShow(true);

  detector.loadVideo("tests/video.avi");
  detector.startVideo();

  return EXIT_SUCCESS;
}


