/**
 * @name: Detector.cpp
 * @author: Roland Godet <r.godet@outlook.fr>
 * @date: 23/01/2020
 */

#include <iostream>
#include "Detector.h"
#include "utility.h"

using namespace std;
using namespace cv;
using namespace cv::aruco;


Detector::Detector() = default;


void Detector::detect() {
  m_markersIds.clear();
  m_markersCorners.clear();
  detectMarkers(m_img, m_dict, m_markersCorners, m_markersIds);
  if (m_markersIds.empty()) debug("Aucun marqueur trouvé");
  else                      debug(to_string(m_markersIds.size()) + " marqueur(s) trouvé(s)");
  if (this->m_show) show();
}


void Detector::loadDict(int dictId) {
  m_dict = getPredefinedDictionary(PREDEFINED_DICTIONARY_NAME(dictId));
  debug("Dictionnaire \"" + to_string(dictId) + "\" chargé");
}


bool Detector::loadImg(const std::string& imgPath) {
  this->m_img = imread(imgPath);
  return testImgLoaded(
      "Impossible de charger l'image \"" + imgPath + "\"",
      "Image \"" + imgPath + "\" chargée");
}


bool Detector::loadImgFromVideo() {
  this->m_video >> this->m_img;
  return testImgLoaded(
      "Impossible de charger l'image à partir de la vidéo",
      "Image chargée à partir de la vidéo");
}


bool Detector::testImgLoaded(const string& errMsg, const string& sucMsg) {
  if (!this->m_img.data) {
    cerr << errMsg << endl;
    debug(errMsg);
    return EXIT_FAILURE;
  }
  debug(sucMsg);
  return EXIT_SUCCESS;
}


bool Detector::loadVideo(const std::string &videoPath) {
  if (!videoPath.empty()) {
    this->m_video.open(videoPath);
    if (this->m_video.isOpened()) {
      debug("Vidéo \"" + videoPath + "\" chargée");
      return EXIT_SUCCESS;
    }
    else {
      cerr << "Impossible de charger la vidéo \"" << videoPath << "\"" << endl;
      debug("Impossible de charger la vidéo \"" + videoPath + "\"");
      return EXIT_FAILURE;
    }
  }
  else {
    this->m_video.open(this->m_cameraId);
    if (this->m_video.isOpened()) {
      debug("Caméra \"" + to_string(this->m_cameraId) + "\" chargée");
      return EXIT_SUCCESS;
    }
    else {
      cerr << "Impossible de charger la caméra \"" << this->m_cameraId << "\"" << endl;
      debug("Impossible de charger la caméra \"" + to_string(this->m_cameraId) + "\"");
      return EXIT_FAILURE;
    }
  }
}


void Detector::setCamera(int id) {
  this->m_cameraId = id;
  debug("Changement de la caméra pour l'identifiant " + to_string(id));
}


void Detector::setShow(bool show) {
  this->m_show = show;
  debug("Changement du paramètre show pour " + to_string(show));
}


void Detector::show(bool pause) {
  Mat cp;
  this->m_img.copyTo(cp);
  if (!m_markersIds.empty())
    drawDetectedMarkers(cp, m_markersCorners, m_markersIds);
  imshow("Resultat", cp);
  if (pause) waitKey(0);
}


bool Detector::startVideo() {
  while (this->m_video.grab()) {
    if (loadImgFromVideo() == EXIT_FAILURE) {
      cerr << "Impossible de charger l'image suivante" << endl;
      debug("Impossible de charger l'image suivante");
      return EXIT_FAILURE;
    }
    detect();
    char key = (char) waitKey(10);
    if (key == 27) break;
  }
}
