/**
 * @name: utility.h
 * @author: Roland Godet <r.godet@outlook.fr>
 * @date: 23/01/2020
 */

#ifndef DETECTION_UTILITY_H
#define DETECTION_UTILITY_H
#define DEBUG

#include <string>


void debug(const std::string& s);


#endif //DETECTION_UTILITY_H
